//
//  GameManager.swift
//  Othello
//
//  Created by Alicja on 24/02/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//

import SpriteKit

class GameManager{
    
    
    var turn: Int! //1- tura zielonych, 2- tura bialych, 3- zadna tura
    
    public func startTheGame(){
        turn = 1
    }
    
    public func getTheTappedStonePosition(fromLocation: CGPoint, width: Double){
        
        let location = fromLocation
        let widthH = (width * 0.95) / 8 //szerokosc komorki
        let x = Double(location.x)
        let y = Double(location.y)
        
        var cellx = 0
        var celly = 0
        
        if(x > 0) && (x < widthH){
            cellx = 4
        }
        else if (x > widthH) && (x < widthH * 2){
            cellx = 5
        }
        else if (x > widthH * 2) && (x < widthH * 3){
            cellx = 6
        }
        else if (x > widthH * 3) && (x < widthH * 4){
            cellx = 7
        }
        else if (x > -widthH) && (x < 0){
            cellx = 3
        }
        else if (x > -widthH * 2) && (x < -widthH){
            cellx = 2
        }
        else if (x > -widthH * 3) && (x < -widthH * 2){
            cellx = 1
        }
        else if (x > -widthH * 4) && (x < -widthH * 3){
            cellx = 0
        }
        
        
        if(y > 0) && (y < widthH){
            celly = 3
        }
        else if (y > widthH) && (y < widthH * 2){
            celly = 2
        }
        else if (y > widthH * 2) && (y < widthH * 3){
            celly = 1
        }
        else if (y > widthH * 3) && (y < widthH * 4){
            celly = 0
        }
        else if (y > -widthH) && (y < 0){
            celly = 4
        }
        else if (y > -widthH * 2) && (y < -widthH){
            celly = 5
        }
        else if (y > -widthH * 3) && (y < -widthH * 2){
            celly = 6
        }
        else if (y > -widthH * 4) && (y < -widthH * 3){
            celly = 7
        }
        
        cellX = celly
        cellY = cellx
        print(cellx)
        print(celly)
        print(" ")
        
        
        
    }
    
    var cellX = 12
    var cellY = 12
    
    private func changeTurn(){
        
        if turn == 1{
            turn = 2
        }
        else if turn == 2{
            turn = 1
        }
        else{
            //TU BEDZIE KONIEC GRY
        }
    }
    
    
}
