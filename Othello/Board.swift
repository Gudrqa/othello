//
//  Board.swift
//  Othello
//
//  Created by Alicja on 24/02/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//

import SpriteKit


class Board{
    
    var gameBoard: SKShapeNode!
    var gameArray: [(node: SKShapeNode, x: Int, y: Int)] = []
    
    var whiteArray: [(node: SKSpriteNode, x:Int, y: Int)] = []
    var greenArray: [(node: SKSpriteNode, x:Int, y: Int)] = []
    var blueArray: [(node: SKSpriteNode, x:Int, y: Int)] = []
    
    var scoreArray = [[Int]](repeating: [Int](repeating: 0, count: 8 ), count: 8) //1- zielony, 2- bialy, 0- nic, 3- mozliwy ruch
    
    
    
    var highScore: SKSpriteNode!
    var reset: SKSpriteNode!
    var settings: SKSpriteNode!
    
    var greenTurn: SKLabelNode!
    var whiteTurn: SKLabelNode!
    
    
    
    public func initializeGameBoard(width1: Double, height2: Double){
        
        let width = width1 * 0.95
        let height = width1 * 0.95
        let rect = CGRect(x: -width / 2, y: -height / 2, width: width, height: height) //wyśrodkowuje
        gameBoard = SKShapeNode(rect: rect, cornerRadius: 0.02 )
        gameBoard.fillColor = SKColor.black
        gameBoard.zPosition = 3
        
        
        let cellWidth: CGFloat = CGFloat(width / 8)
        let numRowsCols = 8
        var x = CGFloat(width / -2) + (cellWidth / 2) //wybranie srodka pierwszej komorki
        var y = CGFloat(height / 2) - (cellWidth / 2)
        
        
        for i in 0...numRowsCols - 1 {
            for j in 0...numRowsCols - 1 {
                let cellNode = SKShapeNode(rectOf: CGSize(width: cellWidth, height: cellWidth))
                let cellStoneGreen = SKSpriteNode(imageNamed: "greenstone")
                let cellStoneWhite = SKSpriteNode(imageNamed: "whitestone")
                let cellStoneBlue = SKSpriteNode(imageNamed: "bluestone")
                cellNode.strokeColor = SKColor.darkGray
                cellNode.position = CGPoint(x:x, y:y) //srodek naszej komorki
                
                cellStoneGreen.zPosition = 3
                cellStoneGreen.position = CGPoint(x:x, y:y)
                
                cellStoneWhite.zPosition = 3
                cellStoneWhite.position = CGPoint(x:x, y:y)
                
                cellStoneBlue.zPosition = 3
                cellStoneBlue.position = CGPoint(x:x, y:y)
                cellStoneBlue.isHidden = true
                cellStoneBlue.name = "blue"
                
                
                if((i == 3 && j == 3) || (i == 4 && j == 4)){
                    
                    cellStoneGreen.isHidden = true
                    scoreArray[i][j] = 2
                    
                }
                else if((i == 3 && j == 4) || (i == 4 && j == 3)){
                    
                    cellStoneWhite.isHidden = true
                    scoreArray[i][j] = 1
                    
                }
                else{
                    cellStoneWhite.isHidden = true
                    cellStoneGreen.isHidden = true
                    scoreArray[i][j] = 0

                }
                
                whiteArray.append((node: cellStoneWhite, x:i, y:j))
                greenArray.append((node: cellStoneGreen, x:i, y:j))
                blueArray.append((node: cellStoneBlue, x:i, y:j))
                gameArray.append((node: cellNode, x:i, y:j)) //wpisanie wszystkich komorek do tabeli 
                gameBoard.addChild(cellNode)
                gameBoard.addChild(cellStoneGreen)
                gameBoard.addChild(cellStoneWhite)
                gameBoard.addChild(cellStoneBlue)
                x += cellWidth
                
            }
            x = CGFloat(width / -2) + (cellWidth / 2)
            y -= cellWidth
        }
        
        reset = SKSpriteNode(imageNamed: "R")
        reset.zPosition = 3
        reset.position = CGPoint(x: (-width / 2) + 200, y: (height / 2) + 90)
        reset.name = "reset_button"
        
        settings = SKSpriteNode(imageNamed: "S")
        settings.zPosition = 3
        settings.position = CGPoint(x: (-width / 2) + 70 , y: (height / 2) + 90)
        settings.name = "settings_button"
        
        greenTurn = SKLabelNode(fontNamed: "Chalkduster")
        greenTurn.zPosition = 4
        greenTurn.position = CGPoint(x: -width/4, y: (-height/2) - 90)
        greenTurn.fontSize = 50
        greenTurn.text = "You"
        greenTurn.fontColor = SKColor.green
        
        whiteTurn = SKLabelNode(fontNamed: "Chalkduster")
        whiteTurn.zPosition = 4
        whiteTurn.position = CGPoint(x: width/4, y: (-height/2) - 90)
        whiteTurn.fontSize = 50
        //whiteTurn.text = "Player Two"
        whiteTurn.fontColor = SKColor.gray
              
        
    }
    
    public func changeThePlayer(turn: Int){ // turn=0 zielone, turn=1 biale, turn=2 brak ruchu
    
        if(turn == 1){
            greenTurn.run(SKAction.colorize(with: .green, colorBlendFactor: 1, duration: 1)){
                
                self.whiteTurn.fontColor = SKColor.green
            }
            whiteTurn.run(SKAction.colorize(with: .gray, colorBlendFactor: 1, duration: 1)){
                
                self.whiteTurn.fontColor = SKColor.gray
            }
        }
        else if(turn == 2){
            
            greenTurn.run(SKAction.colorize(with: .gray, colorBlendFactor: 1, duration: 1)){
                
                self.whiteTurn.fontColor = SKColor.gray
            }
            whiteTurn.run(SKAction.colorize(with: .white, colorBlendFactor: 1, duration: 1)){
                
                self.whiteTurn.fontColor = SKColor.white
            }
            
        }
    
    }
    
    public func clearThePossibilities(){
        for i in 0...7{
            for j in 0...7{
                if scoreArray[i][j] == 3{
                        scoreArray[i][j] = 0
                    for (node, x, y) in blueArray{
                        if (x == i) && (y == j){
                            node.isHidden = true
                        }
                    }
                }
            }
        }
    }
    var scoreGreen: Int!
    var scoreWhite: Int!
    public func endGame(){
        
        scoreGreen = 0
        scoreWhite = 0
        
        for i in 0...7{
            for j in 0...7{
                if scoreArray[i][j] == 1{
                    scoreGreen += 1
                }
                else if scoreArray[i][j] == 2{
                    scoreWhite += 1
                }
            }
        }
        
        
        
    }
    
    var noofpossiblemoves: Int!
    
    public func goThisWay(startX: Int, startY: Int, znajX: Int, znajY: Int){
        
        let sx = startX
        let sy = startY
        let zx = znajX
        let zy = znajY
        
        let walkX = zx - sx
        let walkY = zy - sy
        
        var aa = sx + walkX
        var bb = sy + walkY
        
        while (aa<8 && bb<8 && aa > -1 && bb > -1 && scoreArray[aa][bb] == szukane){
            
            aa += walkX
            bb += walkY
            
        }
        if(aa<8 && bb<8 && aa > -1 && bb > -1){
            
            if (scoreArray[aa][bb] == 0) || (scoreArray[aa][bb] == 3){
                
            }
            else{
                scoreArray[sx][sy] = 3
                showBlueStone(posX: sx, posY: sy)
                noofpossiblemoves += 1
                
            }
        }
        
    }
    
    private func showBlueStone(posX: Int, posY: Int){
        
        for (node, x, y) in blueArray{
            
            if (x == posX) && (y == posY){
                node.isHidden = false
            }
            
        }
        
    }
    
    public func makeAMove(atX: Int, atY: Int, color: Int){
        
        var lookFromX = 0
        var lookFromY = 0
        var lookToX = 0
        var lookToY = 0
        
        if color == 1{
            //daje kamien zielony tam gdzie kliknelam
            for (node, x, y) in greenArray{
                //zamienilam kamien wyjsciowy na kolor
                if (x == atX) && (y == atY){
                    node.isHidden = false
                }
            }
            //usuwam z miejsca gdzie bylam kamien niebieski
            for (node, x, y) in blueArray{
                //zamienilam kamien wyjsciowy na kolor
                if (x == atX) && (y == atY){
                    node.isHidden = true
                }
            }
            //daje w miejscu punkt dla mnie
            scoreArray[atX][atY] = 1
            
                //szukam w ktora strone
                if atX != 0{
                    lookFromX = atX - 1
                }
                else{
                    lookFromX = atX
                }
                if atY != 0{
                    lookFromY = atY - 1
                }
                else{
                    lookFromY = atY
                }
                if atX != 7{
                    lookToX = atX + 1
                }
                else{
                    lookToX = atX
                }
                if atY != 7{
                    lookToY = atY + 1
                }
                else{
                    lookToY = atY
                }
                
                for i in lookFromX ... lookToX{
                    for j in lookFromY...lookToY{
                        if scoreArray[i][j] == 2{
                            let walkX = i - atX
                            let walkY = j - atY
                            
                            var moveX = walkX
                            var moveY = walkY
                            var ile = 1
                            
                            while ((i+moveX < 8 && j+moveY < 8) && (i+moveX > -1 && j+moveY > -1 && scoreArray[i + moveX][j + moveY] == 2) ){
                                moveX += walkX
                                moveY += walkY
                                ile += 1
                            }
                            //jesli po bialych trafimy na zielony to przynajemy punkty
                            if ((i+moveX < 8 && j+moveY < 8) && (i+moveX > -1 && j+moveY > -1 && scoreArray[i+moveX][j+moveY] == 1)){
                                
                                moveX = 0
                                moveY = 0
                                
                                while (ile > 0){
                                    
                                    scoreArray[i + moveX][j + moveY] = 1
                                    
                                    for (node, x, y) in whiteArray{
                                        if (x == i + moveX) && (y == j + moveY){
                                            node.isHidden = true
                                        }
                                    }
                                    for (node, x, y) in greenArray{
                                        if (x == i + moveX) && (y == j + moveY){
                                            node.isHidden = false
                                        }
                                    }
                                    moveX += walkX
                                    moveY += walkY
                                    ile -= 1
                                }
                            }
                        }
                    }
                }
            }
        else if color == 2{
            for (node, x, y) in whiteArray{
                //zamienilam kamien wyjsciowy na kolor
                if (x == atX) && (y == atY){
                    node.isHidden = false
                }
    }
            for (node, x, y) in blueArray{
                //zamienilam kamien wyjsciowy na kolor
                if (x == atX) && (y == atY){
                    node.isHidden = true
                }
            }
            scoreArray[atX][atY]=2
                //szukam w ktora strone
                if atX != 0{
                    lookFromX = atX - 1
                }
                else{
                    lookFromX = atX
                }
                if atY != 0{
                    lookFromY = atY - 1
                }
                else{
                    lookFromY = atY
                }
                if atX != 7{
                    lookToX = atX + 1
                }
                else{
                    lookToX = atX
                }
                if atY != 7{
                    lookToY = atY + 1
                }
                else{
                    lookToY = atY
                }
                
                for i in lookFromX ... lookToX{
                    for j in lookFromY...lookToY{
                        if scoreArray[i][j] == 1{
                            let walkX = i - atX
                            let walkY = j - atY
                            
                            var moveX = walkX
                            var moveY = walkY
                            var ile = 1
                            
                            while ((i+moveX < 8 && j+moveY < 8) && (i+moveX > -1 && j+moveY > -1) && scoreArray[i + moveX][j + moveY] == 1 ){
                                moveX += walkX
                                moveY += walkY
                                ile += 1
                            }
                            //jesli po bialych trafimy na zielony to przynajemy punkty
                            if (i+moveX < 8 && j+moveY < 8 && i+moveX > -1 && j+moveY > -1 && scoreArray[i+moveX][j+moveY] == 2){
                                
                                moveX = 0
                                moveY = 0
                                
                                while (ile > 0){
                                    scoreArray[i + moveX][j + moveY] = 2
                                    for (node, x, y) in whiteArray{
                                        if (x == i + moveX) && (y == j + moveY){
                                            node.isHidden = false
                                        }
                                    }
                                    for (node, x, y) in greenArray{
                                        if (x == i + moveX) && (y == j + moveY){
                                            node.isHidden = true
                                        }
                                    }
                                    moveX += walkX
                                    moveY += walkY
                                    ile -= 1
                                }
                        }
                    }
                }
                
            }
        
    }
        
    }
    
    var szukane: Int!
    
    public func checkTheMoves(kolorOponenta: Int){
        
        szukane = kolorOponenta
        
        noofpossiblemoves = 0
        
        //sprawdz czy pole jest puste
        for i in 0...7{
            for j in 0...7{
                
                //jesli pole jest puste to sprawdzam czy w okolicy sa kamienie przeciwnego koloru 1- tura biala wiec zielonych, 2- tura zielonych wiec bialych
                if scoreArray[i][j] == 0 {
                    
                    
                    if(i==0){
                        //przeszukanie dla rogu lg
                        if(j==0){
                            for a in 0...1{
                                for b in 0...1{
                                    if scoreArray[a][b] == szukane{
                                        goThisWay(startX: i, startY: j, znajX: a, znajY: b)
                                    }
                                }
                            }
                        }
                        //przeszukanie dla rogu pg
                        else if(j==7){
                            for a in 0...1{
                                for b in 6...7{
                                    if scoreArray[a][b] == szukane{
                                        goThisWay(startX: i, startY: j, znajX: a, znajY: b)
                                    }
                                }
                            }
                        }
                        //dla calej krawedzi i=0 ale nie rogow
                        else{
                            for a in 0...1{
                                for b in (j-1)...(j+1){
                                    if scoreArray[a][b] == szukane{
                                        goThisWay(startX: i, startY: j, znajX: a, znajY: b)
                                    }
                                }
                            }
                        }
                    }
                    else if(i==7){
                        //przeszukanie dla rogu ld
                        if(j==0){
                            for a in 6...7{
                                for b in 0...1{
                                    if scoreArray[a][b] == szukane{
                                        goThisWay(startX: i, startY: j, znajX: a, znajY: b)
                                    }
                                }
                            }
                        }
                        //przeszukanie dla rogu pd
                        else if(j==7){
                            for a in 6...7{
                                for b in 6...7{
                                    if scoreArray[a][b] == szukane{
                                        goThisWay(startX: i, startY: j, znajX: a, znajY: b)
                                    }
                                }
                            }
                        }
                        //dla calej krawedzi i=7 ale nie rogow
                        else{
                            for a in 6...7{
                                for b in (j-1)...(j+1){
                                    if scoreArray[a][b] == szukane{
                                        goThisWay(startX: i, startY: j, znajX: a, znajY: b)
                                    }
                                }
                            }
                        }
                    }
                    //dla krawedzi j=0
                    else if (j == 0){
                        for a in (i-1)...(i+1){
                            for b in 0...1{
                                if scoreArray[a][b] == szukane{
                                    goThisWay(startX: i, startY: j, znajX: a, znajY: b)
                                }
                            }
                        }
                    }
                    //dla krawedzi j=7
                    else if (j == 7){
                        for a in (i-1)...(i+1){
                            for b in 6...7{
                                if scoreArray[a][b] == szukane{
                                    goThisWay(startX: i, startY: j, znajX: a, znajY: b)
                                }
                            }
                        }
                    }
                    //inne
                    else{
                        for a in (i-1)...(i+1){
                            for b in (j-1)...(j+1){
                                if scoreArray[a][b] == szukane{
                                    goThisWay(startX: i, startY: j, znajX: a, znajY: b)
                                }
                            }
                        }
                    }
                    
                    
                }
                
            }
            
        }
        
        
        
        
    }
    
    
    
    private func createGameBoard(width: Int, height: Int){
        
    }
    
}
