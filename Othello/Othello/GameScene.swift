//
//  GameScene.swift
//  Othello
//
//  Created by Alicja on 23/02/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//

import SpriteKit
import GameplayKit
import CoreData

class GameScene: SKScene{
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    var gameLogo: SKLabelNode! //labelka z nazwą gry ekran startowy
    var aiButton: SKSpriteNode! // "przycisk" wybierający grę z AI
    var humanButton: SKSpriteNode! // "przycisk" wybierający grę z innym użytkownikiem
    var gameBG: SKShapeNode! //tło zasłaniające napis Hello World
    var highScore: SKSpriteNode! // przycisk do highscore
    
    var playerOne: SKLabelNode!
    var playerTwo: SKLabelNode!
    var enterPlayerOne: UITextField!
    var enterPlayerTwo: UITextField!
    
    var winner: SKLabelNode!
    
    var goButton: SKLabelNode!
    
    var game: GameManager! //instancja klasy GameManager
    var board: Board!
    
    var namePlayerOne: String?
    var namePlayerTwo: String?
    
    var locationOfTap: CGPoint!
    var gameEndedMenu: Bool!
    
    var status: Int = 0
    
    var winnerSymbol: String?
    
    var colorIsGreen: Bool!
    var colorChange: SKLabelNode!
    var colorGreenAndWhite: SKLabelNode!
    var colorWhiteAndBlue: SKLabelNode!
    var backFromSettings: SKLabelNode!
    var settingsOpened: Bool!
    
    
    var highScoreHeader: SKLabelNode!
    var highScoreBestScore: SKLabelNode!
    var highScoreBestScoreUser: SKLabelNode!
    var highScoreYourScore: SKLabelNode!
    var highScoreYourScoreUser: SKLabelNode!
    
    
    override func didMove(to view: SKView) {
        initializeMenu()
        game = GameManager()
        board = Board()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            let location = touch.location(in: self)
            let touchedNode = self.nodes(at: location)
            for node in touchedNode{
                if node.name == "ai_button"{
                    startAIgame() //jesli nacisnie sie na ai button
                    
                }
                else if node.name == "human_button"{
                    startHumangame() //jesli nacisnie sie na human button
                }
                else if node.name == "go_button"{
                    
                    getNames()
                    board.initializeGameBoard(width1: Double(frame.size.width), height2: Double(frame.size.height))
                    hideNickMenu()
                    addAllChildren()
                    board.whiteTurn.text = namePlayerTwo
                    board.greenTurn.text = namePlayerOne
                    game.startTheGame()
                    status = 2
                    board.checkTheMoves(kolorOponenta: 2)
                    
                }
                else if node.name == "reset_button"{
                    board.resetTheGame()
                    self.removeAllActions()
                    self.removeAllChildren()
                    initializeMenu()
                    
                }
                else if node.name == "settings_button"{
                    showSettings()
                }
                else if node.name == "highscore_button"{
                    hideMenu()
                    showHighScore()
                }
                else if node.name == "standardColour"{
                    if(colorIsGreen == false){
                        colorIsGreen = false
                        
                    }else{
                        
                    }
                    
                }
                else if node.name == "secondColour"{
                    if(colorIsGreen == true){
                        colorIsGreen = false
                        
                        board.coloursToWhiteAndBlue()
                        gameBG.fillColor = SKColor.init(red: 18/255, green: 50/255, blue: 36/255, alpha: 1)
                    }
                    
                }
                else if node.name == "backFromSettings"{
                    
                    hideSettings()
                    showGameBoard()
                    
                    
                }
                else if node.name == board.suggestionsColour{
                    
                    locationOfTap = location
                    
                    game.getTheTappedStonePosition(fromLocation: locationOfTap, width: Double(frame.size.width))
                    
                    if (game.turn == 1 && status == 1){
                        
                        board.makeAMove(atX: game.cellX, atY: game.cellY, color: 1)
                        board.clearThePossibilities()
                        game.turn = 2
                        board.changeThePlayer(turn: 2)
                        gameLoop()
                        
                    }
                        //tura zielonych status vs human
                    else if(game.turn == 1 && status == 2){
                        
                        board.makeAMove(atX: game.cellX, atY: game.cellY, color: 1)
                        board.clearThePossibilities()
                        game.turn = 2
                        board.changeThePlayer(turn: 2)
                        
                        board.checkTheMoves(kolorOponenta: 1)
                        
                        if(board.noofpossiblemoves == 0){
                            game.turn = 1
                            board.changeThePlayer(turn: 1)
                            board.checkTheMoves(kolorOponenta: 2)
                            if(board.noofpossiblemoves == 0){
                                gameEnd()
                            }
                            
                        }
                        
                    }
                    else if(game.turn == 2 && status == 2){
                        
                        board.makeAMove(atX: game.cellX, atY: game.cellY, color: 2)
                        board.clearThePossibilities()
                        game.turn = 1
                        board.changeThePlayer(turn: 1)
                        
                        board.checkTheMoves(kolorOponenta: 2)
                        
                        if(board.noofpossiblemoves == 0){
                            game.turn = 2
                            board.changeThePlayer(turn: 2)
                            board.checkTheMoves(kolorOponenta: 1)
                            if(board.noofpossiblemoves == 0){
                                gameEnd()
                            }
                            
                        }
                        
                    }
                
                    
                    //board.changeThePlayer(turn: status % 2)
                    //status += 1
                    
                }
                else{
                    self.view?.endEditing(true)
                    
                    if(gameEndedMenu == true){
                        showHighScore()
                    }
                    if(highScoreMenu == true){
                        highScoreMenu = false
                    }
                    
                }
            }
        }
    }
    
    var highScoreMenu: Bool!
    
    private func showHighScore(){
        highScoreMenu = true
        
        let height = frame.size.height
        
        if(amItheBest == true){
            
            highScoreHeader = SKLabelNode(fontNamed: "SavoyeLetPlain")
            highScoreHeader.zPosition = 3
            highScoreHeader.position = CGPoint(x: 0, y: (height/2) * 0.80 )
            highScoreHeader.fontSize = (height/2) * 0.35
            highScoreHeader.text = "You did it!"
            highScoreHeader.fontColor = SKColor.green
            self.addChild(highScoreHeader)
            
        }
        if(gameEndedMenu == true && amItheBest == false){
            
            highScoreYourScore = SKLabelNode(fontNamed: "Chalkduster")
            highScoreYourScore.zPosition = 3
            highScoreYourScore.position = CGPoint(x: 0, y: (height/2) * -0.20 )
            highScoreYourScore.fontSize = (height/2) * 0.1
            highScoreYourScore.text = "Your Score"
            highScoreYourScore.fontColor = SKColor.green
            self.addChild(highScoreYourScore)
            
            highScoreYourScoreUser = SKLabelNode(fontNamed: "Chalkduster")
            highScoreYourScoreUser.zPosition = 3
            highScoreYourScoreUser.position = CGPoint(x: 0, y: (height/2) * -0.40 )
            highScoreYourScoreUser.fontSize = (height/2) * 0.1
            if winnerColour == "white"{
                highScoreYourScoreUser.text = "\(winnerSymbol)  \(board.scoreWhite):\(board.scoreGreen)"
            }
            else{
                highScoreYourScoreUser.text = "\(winnerSymbol)  \(board.scoreGreen):\(board.scoreWhite)"
            }
            highScoreYourScoreUser.fontColor = SKColor.green
            self.addChild(highScoreYourScoreUser)
            
        }
        
        highScoreBestScore = SKLabelNode(fontNamed: "Chalkduster")
        highScoreBestScore.zPosition = 3
        highScoreBestScore.position = CGPoint(x: 0, y: (height/2) * 0.30 )
        highScoreBestScore.fontSize = (height/2) * 0.1
        highScoreBestScore.text = "Best Score"
        highScoreBestScore.fontColor = SKColor.green
        highScoreBestScore.isHidden = false
        self.addChild(highScoreBestScore)
        
        highScoreBestScoreUser = SKLabelNode(fontNamed: "Chalkduster")
        highScoreBestScoreUser.zPosition = 3
        highScoreBestScoreUser.position = CGPoint(x: 0, y: (height/2) * 0.10 )
        highScoreBestScoreUser.fontSize = (height/2) * 0.05
        
        let nick = UserDefaults.standard.string(forKey: "nick")
        let score = UserDefaults.standard.object(forKey: "stonesFlipped")
        scor
        highScoreBestScoreUser.text = "\(nick)  \(score)"
        
        highScoreBestScoreUser.fontColor = SKColor.green
        
        highScoreBestScore.isHidden = false
        self.addChild(highScoreBestScoreUser)
        
        
        
        
        gameEndedMenu = false
    }
    
    private func changeColor(){
        
        
        
    }
    
    private func showSettings(){
        hideGameBoard()
        
        colorChange.isHidden = false
        colorGreenAndWhite.isHidden = false
        colorWhiteAndBlue.isHidden = false
        backFromSettings.isHidden = false
        settingsOpened = true
        
    }
    
    private func hideSettings(){
        colorChange.isHidden = true
        colorGreenAndWhite.isHidden = true
        colorWhiteAndBlue.isHidden = true
        backFromSettings.isHidden = true
        settingsOpened = false
    }
    
    private func getNames(){
        
        let isPlayerOneEmpty = enterPlayerOne.text?.isEmpty ?? true
        let isPlayerTwoEmpty = enterPlayerTwo.text?.isEmpty ?? true
        
        if(isPlayerOneEmpty){
            namePlayerOne = "Player One"
        }
        else{
            namePlayerOne = enterPlayerOne.text
        }
        if(isPlayerTwoEmpty){
            namePlayerTwo = "Player Two"
        }
        else{
            namePlayerTwo = enterPlayerTwo.text
        }
    }
    
    private func startAIgame(){
        status = 1 ;
        hideMenu()
        board.initializeGameBoard(width1: Double(frame.size.width), height2: Double(frame.size.height))
        addAllChildren()
        board.whiteTurn.text = "AI"
        
        board.changeThePlayer(turn: 1)
        game.startTheGame()
        board.checkTheMoves(kolorOponenta: 2)
        
    }
    
    private func startHumangame(){
        hideMenu()
        showNickMenu()
        
    }
    
    private func gameLoop(){
        
        if(game.turn == 2 && status == 1){ //jesli jest ruch bialych a gra jest AI
            
            board.checkTheMoves(kolorOponenta: 1)
            
            if(board.noofpossiblemoves != 0){
                
                
                var a = Int(arc4random_uniform(7))
                var b = Int(arc4random_uniform(7))
                
                while board.scoreArray[a][b] != 3{
                    a = Int(arc4random_uniform(7))
                    b = Int(arc4random_uniform(7))
                }
                
                board.makeAMove(atX: a, atY: b, color: 2)
                board.clearThePossibilities()
                game.turn = 1
                
                board.changeThePlayer(turn: 1)
                
                board.checkTheMoves(kolorOponenta: 2)
                
                if(board.noofpossiblemoves == 0){
                    game.turn = 2
                    
                    board.changeThePlayer(turn: 2)
                    gameLoop()
                    
                }
            }
            else{
                game.turn = 1
                
                board.changeThePlayer(turn: 1)
                
                board.checkTheMoves(kolorOponenta: 2)
                
                if(board.noofpossiblemoves == 0){
                    gameEnd()
                    
                }
                
            }
            
            
        }
        
    }
    
    private func gameEnd(){
        gameEndedMenu = true
        hideGameBoard()
        board.endGame()
        board.resetTheGame()
        if board.scoreGreen > board.scoreWhite{
            showWinner(whoWon: "green")
        }
        else if board.scoreWhite > board.scoreGreen{
            showWinner(whoWon: "white")
        }
        else{
            showWinner(whoWon: "tie")
        }
        
    }
    
    var amItheBest: Bool!
    var winnerColour: String!
    private func updateScore(forWinnerName: String, forColour: String){
         amItheBest = false
        
        if forColour == "green"{
            if board.scoreGreen > UserDefaults.standard.integer(forKey: "stonesFlipped"){
                UserDefaults.standard.set(board.scoreGreen, forKey: "stonesFlipped")
                UserDefaults.standard.set(forWinnerName, forKey: "nick")
                amItheBest = true
            }
        }
        else{
            if board.scoreWhite > UserDefaults.standard.integer(forKey: "stonesFlipped"){
                UserDefaults.standard.set(board.scoreWhite, forKey: "stonesFlipped")
                UserDefaults.standard.set(forWinnerName, forKey: "nick")
                amItheBest = true
            }
        }
        
        
    }
    
    private func showWinner(whoWon: String){
        
        winnerSymbol = nil
        winner = SKLabelNode(fontNamed: "Chalkduster")
        winner.zPosition = 3
        winner.position = CGPoint(x:0, y: 0)
        winner.fontSize = 100
        winnerColour = whoWon
        
        if(whoWon == "green" && status != 1){
            winner.text = board.greenTurn.text! + " won!"
            winnerSymbol = board.greenTurn.text
            
            updateScore(forWinnerName: winnerSymbol!, forColour: whoWon)
        }
        else if(whoWon == "white" && status != 1){
            winner.text = board.whiteTurn.text! + " won!"
            winnerSymbol = board.whiteTurn.text
            updateScore(forWinnerName: winnerSymbol!, forColour: whoWon)
        }
        else if (whoWon == "white" && status == 1){
            winner.text = "You lost!"
        }
        else if(whoWon == "green" && status == 1){
            winner.text = "You won!"
            winnerSymbol = board.greenTurn.text
            updateScore(forWinnerName: winnerSymbol!, forColour: whoWon)
        }
        else{
            winner.text = "It's a tie!"
        }
        
        winner.fontColor = SKColor.green
        self.addChild(winner)
        
        
    }
    
    private func addAllChildren(){
        
        self.addChild(board.gameBoard)
        self.addChild(board.reset)
        self.addChild(board.settings)
        self.addChild(board.greenTurn)
        self.addChild(board.whiteTurn)
        board.settings.name = "settings_button"
        board.reset.name = "reset_button"
        
    }
    
    private func hideGameBoard(){
        board.gameBoard.isHidden = true
        board.reset.isHidden = true
        board.settings.isHidden = true
        board.greenTurn.isHidden = true
        board.whiteTurn.isHidden = true
        
    }
    
    private func showGameBoard(){
        board.gameBoard.isHidden = false
        board.reset.isHidden = false
        board.settings.isHidden = false
        board.greenTurn.isHidden = false
        board.whiteTurn.isHidden = false
        
    }
    
    private func hideNickMenu(){
        
        playerOne.run(SKAction.fadeOut(withDuration: 1.0)){
            self.playerOne.isHidden = true
            self.enterPlayerOne.isHidden = true
        }
        
        enterPlayerOne.isHidden = true
        enterPlayerTwo.isHidden = true
        
        playerTwo.run(SKAction.fadeOut(withDuration: 1.0)){
            self.aiButton.isHidden = true
            self.enterPlayerTwo.isHidden = true
        }
        goButton.run(SKAction.fadeOut(withDuration: 1.0)){
            self.goButton.isHidden = true
        }
    }
    
    private func hideMenu(){
        gameLogo.run(SKAction.fadeOut(withDuration: 1.0)){
            self.gameLogo.isHidden = true
        }
        
        aiButton.run(SKAction.scale(to: 0, duration: 1.0)){
            self.aiButton.isHidden = true
        }
        humanButton.run(SKAction.scale(to: 0, duration: 1.0)){
            self.humanButton.isHidden = true
        }
        
        highScore.isHidden = true
    }

    private func showNickMenu(){
        playerOne.run(SKAction.scale(to: 1, duration: 1.0)){
            self.playerOne.isHidden = false
            self.enterPlayerOne.isHidden = false
            self.playerTwo.isHidden = false
            self.enterPlayerTwo.isHidden = false
            self.goButton.isHidden = false
        }
    }

    
    private func initializeMenu(){
        
        amItheBest = false
        
        gameEndedMenu = false
        //zasloniecie hello word
        let width = frame.size.width
        let height = frame.size.height
        let rect = CGRect(x: -width/2 , y: -height/2, width: width, height: height)
        gameBG = SKShapeNode(rect: rect)
        gameBG.fillColor = SKColor.black
        gameBG.zPosition = 2
        gameBG.isHidden = false
        self.addChild(gameBG)
        
        //logo gry
        gameLogo = SKLabelNode(fontNamed: "SavoyeLetPlain")
        gameLogo.zPosition = 3
        gameLogo.position = CGPoint(x: 0, y: (height/2) * 0.60 )
        gameLogo.fontSize = (height/2) * 0.35
        gameLogo.text = "Othello"
        gameLogo.fontColor = SKColor.green
        self.addChild(gameLogo)
        
        //przycisk graj z AI
        aiButton = SKSpriteNode(imageNamed: "vsAI")
        aiButton.zPosition = 3
        aiButton.position = CGPoint(x: 0, y: (height/2) * 0.15)
        aiButton.name = "ai_button"
        self.addChild(aiButton)
        
        //przycisk graj z Czlowiekiem
        humanButton = SKSpriteNode(imageNamed: "vsHuman")
        humanButton.zPosition = 3
        humanButton.position = CGPoint(x: 0, y: (height/2) * -0.15)
        humanButton.name = "human_button"
        self.addChild(humanButton)
        
        highScore = SKSpriteNode(imageNamed: "H")
        highScore.zPosition = 3
        highScore.position = CGPoint(x: (width / 2) - 70, y: (-height / 2) + 70)
        highScore.name = "highscore_button"
        highScore.isHidden = false
        self.addChild(highScore)
        
        
        //to bedzie w menu dalej
        playerOne = SKLabelNode(fontNamed: "SavoyeLetPlain")
        playerOne.zPosition = 3
        playerOne.position = CGPoint(x:0, y: (height/2) * 0.6)
        playerOne.fontSize = (height/2) * 0.2
        playerOne.text = "Player One"
        playerOne.fontColor = SKColor.green
        playerOne.isHidden = true
        self.addChild(playerOne)
        
        enterPlayerOne = UITextField(frame: CGRect(x: 0, y: (height/2)*0.2, width: width * 0.5 , height: height * 0.1))
        enterPlayerOne.textColor = SKColor.green
        enterPlayerOne.font = UIFont(name: "Chalkduster", size: height * 0.027)
        enterPlayerOne.adjustsFontSizeToFitWidth = true
        enterPlayerOne.backgroundColor = UIColor.black
        enterPlayerOne.attributedPlaceholder = NSAttributedString(string: "enter name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.green])
        enterPlayerOne.textAlignment = NSTextAlignment.center
        enterPlayerOne.isHidden = true
        self.view!.addSubview(enterPlayerOne)
        
        playerTwo = SKLabelNode(fontNamed: "SavoyeLetPlain")
        playerTwo.zPosition = 3
        playerTwo.position = CGPoint(x:0, y: 0)
        playerTwo.fontSize = (height/2) * 0.2
        playerTwo.text = "Player Two "
        playerTwo.fontColor = SKColor.white
        playerTwo.isHidden = true
        self.addChild(playerTwo)
        
        enterPlayerTwo = UITextField(frame: CGRect(x: 0, y: (height/2) * 0.50 ,width: width * 0.5 , height: height * 0.1))
        enterPlayerTwo.textColor = SKColor.white
        enterPlayerTwo.font = UIFont(name: "Chalkduster", size: height * 0.027)
        enterPlayerTwo.adjustsFontSizeToFitWidth = true
        enterPlayerTwo.backgroundColor = UIColor.black
        enterPlayerTwo.attributedPlaceholder = NSAttributedString(string: "enter name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        enterPlayerTwo.textAlignment = NSTextAlignment.center
        enterPlayerTwo.isHidden = true
        self.view!.addSubview(enterPlayerTwo)
        
        goButton = SKLabelNode(fontNamed: "Chalkduster")
        goButton.zPosition = 3
        goButton.position = CGPoint(x:0, y: (height/2) * -0.7)
        goButton.fontSize = (height/2) * 0.2
        goButton.text = "GO!"
        goButton.fontColor = SKColor.gray
        goButton.isHidden = true
        goButton.name = "go_button"
        self.addChild(goButton)
        
        //settings menu
        colorChange = SKLabelNode(fontNamed: "Chalkduster")
        colorChange.zPosition = 3
        colorChange.position = CGPoint(x:0, y: (height/2) * 0.6)
        colorChange.fontSize = (height/2) * 0.08
        colorChange.text = "Choose your colour: "
        colorChange.fontColor = SKColor.green
        colorChange.isHidden = true
        self.addChild(colorChange)
        
        colorIsGreen = true
        settingsOpened = true
        
        colorGreenAndWhite = SKLabelNode(fontNamed: "Chalkduster")
        colorGreenAndWhite.zPosition = 3
        colorGreenAndWhite.position = CGPoint(x:0, y: (height/2) * 0.2)
        colorGreenAndWhite.fontSize = (height/2) * 0.08
        colorGreenAndWhite.text = "Green and White"
        colorGreenAndWhite.fontColor = SKColor.green
        colorGreenAndWhite.name = "standardColour"
        colorGreenAndWhite.isHidden = true
        self.addChild(colorGreenAndWhite)
        
        colorWhiteAndBlue = SKLabelNode(fontNamed: "Chalkduster")
        colorWhiteAndBlue.zPosition = 3
        colorWhiteAndBlue.position = CGPoint(x:0, y: (height/2) * -0.2)
        colorWhiteAndBlue.fontSize = (height/2) * 0.08
        colorWhiteAndBlue.text = "White and Blue"
        colorWhiteAndBlue.fontColor = SKColor.white
        colorWhiteAndBlue.name = "secondColour"
        colorWhiteAndBlue.isHidden = true
        self.addChild(colorWhiteAndBlue)
        
        backFromSettings = SKLabelNode(fontNamed: "Chalkduster")
        backFromSettings.zPosition = 3
        backFromSettings.position = CGPoint(x:0, y: (height/2) * -0.6)
        backFromSettings.fontSize = (height/2) * 0.1
        backFromSettings.text = "Back"
        backFromSettings.fontColor = SKColor.white
        backFromSettings.name = "backFromSettings"
        backFromSettings.isHidden = true
        self.addChild(backFromSettings)
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
