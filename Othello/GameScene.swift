//
//  GameScene.swift
//  Othello
//
//  Created by Alicja on 23/02/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//

import SpriteKit
import GameplayKit
import CoreData

class GameScene: SKScene{
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    var gameLogo: SKLabelNode! //labelka z nazwą gry ekran startowy
    var aiButton: SKSpriteNode! // "przycisk" wybierający grę z AI
    var humanButton: SKSpriteNode! // "przycisk" wybierający grę z innym użytkownikiem
    var gameBG: SKShapeNode! //tło zasłaniające napis Hello World
    var highScore: SKSpriteNode! // przycisk do highscore
    
    var playerOne: SKLabelNode!
    var playerTwo: SKLabelNode!
    var enterPlayerOne: UITextField!
    var enterPlayerTwo: UITextField!
    
    var winner: SKLabelNode!
    
    var goButton: SKLabelNode!
    
    var game: GameManager! //instancja klasy GameManager
    var board: Board!
    
    var namePlayerOne: String?
    var namePlayerTwo: String?
    
    override func didMove(to view: SKView) {
        initializeMenu()
        game = GameManager()
        board = Board()
    }
    
    var locationOfTap: CGPoint!
    
    var gameEndedMenu: Bool!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            let location = touch.location(in: self)
            let touchedNode = self.nodes(at: location)
            for node in touchedNode{
                if node.name == "ai_button"{
                    startAIgame() //jesli nacisnie sie na ai button
                    
                }
                else if node.name == "human_button"{
                    startHumangame() //jesli nacisnie sie na human button
                }
                else if node.name == "go_button"{
                    
                    getNames()
                    board.initializeGameBoard(width1: Double(frame.size.width), height2: Double(frame.size.height))
                    hideNickMenu()
                    addAllChildren()
                    board.whiteTurn.text = namePlayerTwo
                    board.greenTurn.text = namePlayerOne
                    game.startTheGame()
                    status = 2
                    board.checkTheMoves(kolorOponenta: 2)
                    
                }
                else if node.name == "reset_button"{
                    self.removeAllActions()
                    self.removeAllChildren()
                    initializeMenu()
                    game = GameManager()
                    board = Board()
                    
                }
                else if node.name == "settings_button"{
                    hideGameBoard()
                }
                else if node.name == "highscore_button"{
                    hideMenu()
                }
                else if node.name == "blue"{
                    
                    locationOfTap = location
                    
                    game.getTheTappedStonePosition(fromLocation: locationOfTap, width: Double(frame.size.width))
                    
                    if (game.turn == 1 && status == 1){
                        
                        board.makeAMove(atX: game.cellX, atY: game.cellY, color: 1)
                        board.clearThePossibilities()
                        game.turn = 2
                        board.changeThePlayer(turn: 2)
                        gameLoop()
                        
                    }
                        //tura zielonych status vs human
                    else if(game.turn == 1 && status == 2){
                        
                        board.makeAMove(atX: game.cellX, atY: game.cellY, color: 1)
                        board.clearThePossibilities()
                        game.turn = 2
                        board.changeThePlayer(turn: 2)
                        
                        board.checkTheMoves(kolorOponenta: 1)
                        
                        if(board.noofpossiblemoves == 0){
                            game.turn = 1
                            board.changeThePlayer(turn: 1)
                            board.checkTheMoves(kolorOponenta: 2)
                            if(board.noofpossiblemoves == 0){
                                gameEnd()
                            }
                            
                        }
                        
                    }
                    else if(game.turn == 2 && status == 2){
                        
                        board.makeAMove(atX: game.cellX, atY: game.cellY, color: 2)
                        board.clearThePossibilities()
                        game.turn = 1
                        board.changeThePlayer(turn: 1)
                        
                        board.checkTheMoves(kolorOponenta: 2)
                        
                        if(board.noofpossiblemoves == 0){
                            game.turn = 2
                            board.changeThePlayer(turn: 2)
                            board.checkTheMoves(kolorOponenta: 1)
                            if(board.noofpossiblemoves == 0){
                                gameEnd()
                            }
                            
                        }
                        
                    }
                
                    
                    //board.changeThePlayer(turn: status % 2)
                    //status += 1
                    
                }
                else{
                    self.view?.endEditing(true)
                    
                    if(gameEndedMenu == true){
                        
                    }
                    
                }
            }
        }
    }
    
    var status: Int = 0
    
    
    private func getNames(){
        
        if(enterPlayerOne.text == nil){
            namePlayerOne = "Player One"
        }
        else{
            namePlayerOne = enterPlayerOne.text
        }
        if(enterPlayerTwo.text == nil){
            namePlayerTwo = "Player Two"
        }
        else{
            namePlayerTwo = enterPlayerTwo.text
        }
    }
    
    private func startAIgame(){
        status = 1 ;
        hideMenu()
        board.initializeGameBoard(width1: Double(frame.size.width), height2: Double(frame.size.height))
        addAllChildren()
        board.whiteTurn.text = "AI"
        
        board.changeThePlayer(turn: 1)
        game.startTheGame()
        board.checkTheMoves(kolorOponenta: 2)
        
    }
    
    private func startHumangame(){
        hideMenu()
        showNickMenu()
        
    }
    
    private func gameLoop(){
        
        if(game.turn == 2 && status == 1){ //jesli jest ruch bialych a gra jest AI
            
            board.checkTheMoves(kolorOponenta: 1)
            
            if(board.noofpossiblemoves != 0){
                
                
                var a = Int(arc4random_uniform(7))
                var b = Int(arc4random_uniform(7))
                
                while board.scoreArray[a][b] != 3{
                    a = Int(arc4random_uniform(7))
                    b = Int(arc4random_uniform(7))
                }
                
                board.makeAMove(atX: a, atY: b, color: 2)
                board.clearThePossibilities()
                game.turn = 1
                
                board.changeThePlayer(turn: 1)
                
                board.checkTheMoves(kolorOponenta: 2)
                
                if(board.noofpossiblemoves == 0){
                    game.turn = 2
                    
                    board.changeThePlayer(turn: 2)
                    gameLoop()
                    
                }
            }
            else{
                game.turn = 1
                
                board.changeThePlayer(turn: 1)
                
                board.checkTheMoves(kolorOponenta: 2)
                
                if(board.noofpossiblemoves == 0){
                    gameEnd()
                    
                }
                
            }
            
            
        }
        
    }
    
    private func gameEnd(){
        gameEndedMenu = true
        hideGameBoard()
        board.endGame()
        if board.scoreGreen > board.scoreWhite{
            showWinner(whoWon: "green")
        }
        else if board.scoreWhite > board.scoreGreen{
            showWinner(whoWon: "white")
        }
        else{
            showWinner(whoWon: "tie")
        }
        
    }
    
    var winnerSymbol: String?
    
    private func showWinner(whoWon: String){
        
        winnerSymbol = nil
        winner = SKLabelNode(fontNamed: "Chalkduster")
        winner.zPosition = 3
        winner.position = CGPoint(x:0, y: 0)
        winner.fontSize = 100
        if(whoWon == "green" && status != 1){
            winner.text = board.greenTurn.text! + " won!"
            winnerSymbol = board.greenTurn.text
        }
        else if(whoWon == "white" && status != 1){
            winner.text = board.whiteTurn.text! + " won!"
            winnerSymbol = board.whiteTurn.text
        }
        else if (whoWon == "white" && status == 1){
            winner.text = "You lost!"
        }
        else if(whoWon == "green" && status == 1){
            winner.text = "You won!"
            winnerSymbol = board.greenTurn.text
        }
        else{
            winner.text = "It's a tie!"
        }
        
        winner.fontColor = SKColor.green
        self.addChild(winner)
        
    }
    
    
    private func addAllChildren(){
        
        self.addChild(board.gameBoard)
        self.addChild(board.reset)
        self.addChild(board.settings)
        self.addChild(board.greenTurn)
        self.addChild(board.whiteTurn)
        board.settings.name = "settings_button"
        board.reset.name = "reset_button"
        
    }
    
    private func hideGameBoard(){
        board.gameBoard.isHidden = true
        board.reset.isHidden = true
        board.settings.isHidden = true
        board.greenTurn.isHidden = true
        board.whiteTurn.isHidden = true
        
    }
    
    private func hideNickMenu(){
        
        playerOne.run(SKAction.fadeOut(withDuration: 1.0)){
            self.playerOne.isHidden = true
            self.enterPlayerOne.isHidden = true
        }
        
        enterPlayerOne.isHidden = true
        enterPlayerTwo.isHidden = true
        
        playerTwo.run(SKAction.fadeOut(withDuration: 1.0)){
            self.aiButton.isHidden = true
            self.enterPlayerTwo.isHidden = true
        }
        goButton.run(SKAction.fadeOut(withDuration: 1.0)){
            self.goButton.isHidden = true
        }
    }
    
    
    
    private func hideMenu(){
        gameLogo.run(SKAction.fadeOut(withDuration: 1.0)){
            self.gameLogo.isHidden = true
        }
        
        aiButton.run(SKAction.scale(to: 0, duration: 1.0)){
            self.aiButton.isHidden = true
        }
        humanButton.run(SKAction.scale(to: 0, duration: 1.0)){
            self.humanButton.isHidden = true
        }
        
        highScore.isHidden = true
    }

    
    private func showNickMenu(){
        playerOne.run(SKAction.scale(to: 1, duration: 1.0)){
            self.playerOne.isHidden = false
            self.enterPlayerOne.isHidden = false
            self.playerTwo.isHidden = false
            self.enterPlayerTwo.isHidden = false
            self.goButton.isHidden = false
        }
    }
    
    private func showHighScore(){
//
//
//        //coreData
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//        let entity = NSEntityDescription.entity(forEntityName: "Players", in: context)
//        let newUser = NSManagedObject(entity: entity!, insertInto: context)
//        
//        newUser.setValue(winnerSymbol, forKey: "nick")
//        do {
//            try context.save()
//        } catch  {
//            print("failed saving")
//        }
        
    }
    
    
    private func initializeMenu(){
        
        
        
        gameEndedMenu = false
        //zasloniecie hello word
        let width = frame.size.width
        let height = frame.size.height
        let rect = CGRect(x: -width/2 , y: -height/2, width: width, height: height)
        gameBG = SKShapeNode(rect: rect)
        gameBG.fillColor = SKColor.black
        gameBG.zPosition = 2
        gameBG.isHidden = false
        self.addChild(gameBG)
        
        //logo gry
        gameLogo = SKLabelNode(fontNamed: "SavoyeLetPlain")
        gameLogo.zPosition = 3
        gameLogo.position = CGPoint(x: 0, y: (height/2) - 200)
        gameLogo.fontSize = 200
        gameLogo.text = "Othello"
        gameLogo.fontColor = SKColor.green
        self.addChild(gameLogo)
        
        //przycisk graj z AI
        aiButton = SKSpriteNode(imageNamed: "vsAI")
        aiButton.zPosition = 3
        aiButton.position = CGPoint(x: 0, y: (height/2) - 600)
        aiButton.name = "ai_button"
        self.addChild(aiButton)
        
        //przycisk graj z Czlowiekiem
        humanButton = SKSpriteNode(imageNamed: "vsHuman")
        humanButton.zPosition = 3
        humanButton.position = CGPoint(x: 0, y: (height/2) - 800)
        humanButton.name = "human_button"
        self.addChild(humanButton)
        
        //to bedzie w menu dalej
        playerOne = SKLabelNode(fontNamed: "SavoyeLetPlain")
        playerOne.zPosition = 3
        playerOne.position = CGPoint(x:0, y: (height/2) - 400)
        playerOne.fontSize = 100
        playerOne.text = "Player One"
        playerOne.fontColor = SKColor.green
        playerOne.isHidden = true
        self.addChild(playerOne)
        
        highScore = SKSpriteNode(imageNamed: "H")
        highScore.zPosition = 3
        highScore.position = CGPoint(x: (width / 2) - 70, y: (-height / 2) + 70)
        highScore.name = "highscore_button"
        highScore.isHidden = false
        self.addChild(highScore)
        
        enterPlayerOne = UITextField(frame: CGRect(x: 90, y: (height/2) - 450, width: 200, height: 50))
        enterPlayerOne.textColor = SKColor.green
        enterPlayerOne.font = UIFont(name: "Chalkduster", size: 30)
        enterPlayerOne.adjustsFontSizeToFitWidth = true
        enterPlayerOne.backgroundColor = UIColor.black
        enterPlayerOne.attributedPlaceholder = NSAttributedString(string: "enter name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.green])
        enterPlayerOne.textAlignment = NSTextAlignment.center
        enterPlayerOne.isHidden = true
        self.view!.addSubview(enterPlayerOne)
        
        playerTwo = SKLabelNode(fontNamed: "SavoyeLetPlain")
        playerTwo.zPosition = 3
        playerTwo.position = CGPoint(x:0, y: (height/2) - 690)
        playerTwo.fontSize = 100
        playerTwo.text = "Player Two "
        playerTwo.fontColor = SKColor.white
        playerTwo.isHidden = true
        self.addChild(playerTwo)
        
        enterPlayerTwo = UITextField(frame: CGRect(x: 90, y: (height/2) - 300, width: 200, height: 50))
        enterPlayerTwo.textColor = SKColor.white
        enterPlayerTwo.font = UIFont(name: "Chalkduster", size: 30)
        enterPlayerTwo.adjustsFontSizeToFitWidth = true
        enterPlayerTwo.backgroundColor = UIColor.black
        enterPlayerTwo.attributedPlaceholder = NSAttributedString(string: "enter name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        enterPlayerTwo.textAlignment = NSTextAlignment.center
        enterPlayerTwo.isHidden = true
        self.view!.addSubview(enterPlayerTwo)
        
        goButton = SKLabelNode(fontNamed: "Chalkduster")
        goButton.zPosition = 3
        goButton.position = CGPoint(x:0, y: (height/2) - 1000)
        goButton.fontSize = 100
        goButton.text = "GO!"
        goButton.fontColor = SKColor.gray
        goButton.isHidden = true
        goButton.name = "go_button"
        self.addChild(goButton)
    }
    
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
